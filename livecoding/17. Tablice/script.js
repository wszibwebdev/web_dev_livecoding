var numericArray = [1, 2, 3];
console.log(numericArray);
var textArray = ['jeden', 'dwa', 'trzy'];
console.log(textArray);
var booleanArray = [true, false, false];
console.log(booleanArray);
var array = [1, 'jeden', true];
console.log(array);

var a = 5;
var b = 3;
var c = "2";

var sum = a + b + c;
console.log('Wynik sumowania:')
console.log(sum);

/* ---------------------------- */

console.log(numericArray[1]);
numericArray[2] = 100;
console.log(numericArray);
console.log(numericArray[-1]);

var isInArray = numericArray.includes(3);
var isInArray2 = numericArray.includes(100);

console.log('Czy jest w tablicy wartość 3: ' + isInArray);
console.log('Czy jest w tablicy wartość 100: ' + isInArray2);

numericArray.push(10);
console.log(numericArray);