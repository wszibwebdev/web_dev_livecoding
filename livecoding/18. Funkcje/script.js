function simpleFunction() {
    console.log('Jakiś tekst');
}

simpleFunction();

function rewrite(text) {
    console.log(text);
}

rewrite('Jakiś tekst w funkcji');

function sumOfTwo(a, b) {
    var sum = a + b;
    console.log('Suma tych liczb to: ' + sum);
}

sumOfTwo(2, 2);

function functionWithReturn() {
    return 'Jakas zwrocona wartosc';
}

var fromFunction = functionWithReturn();
console.log(fromFunction)