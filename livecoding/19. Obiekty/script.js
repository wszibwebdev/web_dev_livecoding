/* notacja literałów */

var person = {
    firstName: 'Jan',
    lastName: 'Kowalski',
    introduce: function () {
        console.log('Nazywam się: ' + this.firstName + ' ' + this.lastName);
    }
}

person.introduce();

/* notacja konstruktora */

function Car(brand, model) {
    this.brand = brand;
    this.model = model;
    this.showDetails = function () {
        console.log('Car - ' + this.brand + ' ' + this.model);
    };
}

var fiat = new Car('Fiat', '126p');
fiat.showDetails();

/* obiekt poprzez klase */

class House {
    constructor(windows, doors) {
        this.windows = windows;
        this.doors = doors;
    }
    showDetails() {
        console.log('The house has ' + this.windows + ' windows and ' + this.doors + ' doors');
    }
}

var house = new House(5, 10);
house.showDetails();

var doorsInHouse = house.doors;
console.log(doorsInHouse);

house.windows = 15;
console.log(house);
