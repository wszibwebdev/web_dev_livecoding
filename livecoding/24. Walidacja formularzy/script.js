window.onload = function () {
    var usernameInput = document.getElementById('username');
    var existUserNames = ['admin', 'user'];

    function validateUsername() {
        var validUsername = true;

        if (!checkRequiredInput(usernameInput)) {
            validUsername = false;
            setErrorText(usernameInput, 'Podaj nazwę użytkownika');
        } else if (!checkMinMaxLength(usernameInput, 3, 12)) {
            validUsername = false;
            setErrorText(usernameInput, 'Nazwa użytkownika powinna mieć długość pomiędzy 3 a 12 znaków');
        } else if (checkExsitUsername(usernameInput)) {
            validUsername = false;
            setErrorText(usernameInput, 'Nazwa użytkownika została zabroniona');
        } else {
            hideError(usernameInput);
        }
        return validUsername;
    }

    function checkRequiredInput(input) {
        return input.value.length > 0;
    }

    function checkMinMaxLength(input, min, max) {
        return input.value.length >= min && input.value.length <= max;
    }

    function checkExsitUsername(input) {
        return existUserNames.includes(input.value);
    }

    function setErrorText(input, text) {
        input.nextElementSibling.textContent = text;
        input.nextElementSibling.removeAttribute('hidden');
    }

    function hideError(input) {
        input.nextElementSibling.setAttribute('hidden', '');
    }

    document.getElementById('form').addEventListener('submit', function (e) {
        e.preventDefault();
        if (validateUsername()) {
            window.alert('Poprawnie wysłąny formularz');
        }
    });






};