$(function () {

    var clickCounter = 0;
    var dblClickCounter = 0;
    var keyDownCounter = 0;

    /*var button = document.getElementById('add-element');*/
    var button = $('#add-element');
    /*var boxContainer = document.getElementById('box-container');*/
    var boxContainer = $('#box-container');

    /*document.addEventListener('click', function () {
        document.getElementById('click-counter').textContent = ++clickCounter;
    });*/
    $(document).on('click', function () {
        $('#click-counter').text(++clickCounter);
    });

    /*document.addEventListener('dblclick', function () {
        document.getElementById('dbl-click-counter').textContent = ++dblClickCounter;
    });*/
    $(document).on('dblclick', function () {
        $('#dbl-click-counter').text(++dblClickCounter);
    });

    /*document.addEventListener('keydown', function () {
        document.getElementById('key-down-counter').textContent = ++keyDownCounter;
    });*/

    $(document).on('keydown', function () {
        $('#key-down-counter').text(++keyDownCounter);
    });

    var boxCounter = 0;

    /*button.addEventListener('click', function () {
        boxCounter++;
        var newElement = document.createElement('div');
        newElement.innerText = boxCounter.toString();
        newElement.classList.add('box');
        boxContainer.appendChild(newElement);
    });*/

    button.on('click', function () {
        boxCounter++;
        var newElement = $('<div class="box">' + boxCounter + '</div>');
        boxContainer.append(newElement);
    });

    /*boxContainer.addEventListener('click', function (e) {
        if (e.target.className.includes('box')) {
            this.removeChild(e.target);
        }
    });*/

    boxContainer.on('click', '.box', function () {
        $(this).remove();
    });


    document.addEventListener('click', function (e) {
        var newElement = document.createElement('div');
        newElement.style.width = '5px';
        newElement.style.height = '5px';
        newElement.style.backgroundColor = 'red';
        newElement.style.position = 'absolute';
        newElement.style.left = e.clientX.toString() + 'px';
        newElement.style.top = e.clientY.toString() + 'px';
        document.body.appendChild(newElement);
    });




});